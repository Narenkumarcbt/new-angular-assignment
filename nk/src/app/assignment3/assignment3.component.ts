import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-assignment3',
  templateUrl: './assignment3.component.html',
  styleUrls: ['./assignment3.component.css']
})
export class Assignment3Component implements OnInit {
  showsecret= false;
  log :any[]= [];
    
  onToggleDetails() {
    this.showsecret= !this.showsecret;
    this.log.push(this.log.length+1);
  }
  

  constructor() { }

  ngOnInit(): void {
  }

}
