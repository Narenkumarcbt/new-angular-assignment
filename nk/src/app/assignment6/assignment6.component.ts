import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-assignment6',
  templateUrl: './assignment6.component.html',
  styleUrls: ['./assignment6.component.css']
})
export class Assignment6Component implements OnInit {

  firstname!: string;
  lastname!: string;
  defaultCountry: string = 'india';
  email!: string;
  gen!: string;
  country!: string;
  gender = [
    { id: 1, value: 'male' },
    { id: 2, value: 'female' },
    { id: 3, value: 'others' },
  ];
  defaultgender: string = 'male';
  constructor() {}
  @ViewChild('myform') form!: NgForm;

  onsubmit(form: NgForm) {
    console.log(this.form);
    this.firstname = this.form.value.personalDetails.firstname;
    this.lastname = this.form.value.personalDetails.lastname;
    this.email = this.form.value.personalDetails.email;
    this.gen = this.form.value.gender;
    this.country = this.form.value.country;
    this.form.reset();
  }
  // onsubmit(form: NgForm) {
  //   console.log(form);
  // }
  defaultValues() {
    // this.form.setValue({                                        setValueMethode
    //   country: '',
    //   gender: '',
    //   hobbies: '',
    //   personalDetails: {
    //     email: 'codeboardtech@gmail.com',
    //     firstname: 'codeboard',
    //     lastname: 'Technologies',
    //     },  });

    this.form.form.patchValue({
      personalDetails: {
        email: 'codeboardtech@gmail.com',
        firstname: 'codeboard',
        lastname: 'Technologies',
      },
    });
  }
  ngOnInit(): void {}

}
