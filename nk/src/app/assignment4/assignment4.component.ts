import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-assignment4',
  templateUrl: './assignment4.component.html',
  styleUrls: ['./assignment4.component.css']
})
export class Assignment4Component implements OnInit {
  // @Input() number!:any;
  evennumbers:number[]=[];
  oddnumber:number[]=[];
  oninterval(event:number){
if(event%2==0){
  this.evennumbers.push(event)
}else{
  this.oddnumber.push(event)
}
  }
  

  constructor() { }

  ngOnInit(): void {
  }
   

}
