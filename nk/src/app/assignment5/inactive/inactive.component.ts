import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-inactive',
  templateUrl: './inactive.component.html',
  styleUrls: ['./inactive.component.css']
})
export class InactiveComponent implements OnInit {

  constructor(private serve:UsersService) { }
inactive:string[]=[];
  ngOnInit(): void {
    this.inactive=this.serve.inactive;
  }
deactive(value:any){
this.serve.deactivate(value)
}
}
