import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-active',
  templateUrl: './active.component.html',
  styleUrls: ['./active.component.css']
})
export class ActiveComponent implements OnInit {

  constructor(private serve:UsersService) { }
active:string[]=[];
  ngOnInit(): void {
    this.active=this.serve.active;
  }
deactive(value:any){
  this.serve.activework(value)
}
}
