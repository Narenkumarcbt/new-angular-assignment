import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
	import { warningAlertComponent } from './warning-alert/warning-alert-component';
import { SuccessAlertComponent } from './success-alert/success-alert.component';
import { AssignmentComponent } from './assignment/assignment.component';
import { Assignment3Component } from './assignment3/assignment3.component';
import { Assignment4Component } from './assignment4/assignment4.component';
import { Assignment5Component } from './assignment5/assignment5.component';
import { GameControlComponent } from './assignment4/game-control/game-control.component';
import { EvenComponent } from './assignment4/even/even.component';
import { OddComponent } from './assignment4/odd/odd.component';
import { AppComponent } from './app.component';
import { ActiveComponent } from './assignment5/active/active.component';
import { InactiveComponent } from './assignment5/inactive/inactive.component';
import { Assignment6Component } from './assignment6/assignment6.component';
import { Assignment7Component } from './assignment7/assignment7.component';
import { ReactiveFormsModule } from '@angular/forms';
import { Assignment8Component } from './assignment8/assignment8.component';
import { ChaskTsPipe } from './chask.ts.pipe';


@NgModule({
  declarations: [
    AppComponent,
    warningAlertComponent,
    SuccessAlertComponent,
    AssignmentComponent,
    Assignment3Component,
    Assignment4Component,
    GameControlComponent,
    OddComponent,
    EvenComponent,
    Assignment5Component,
    GameControlComponent,
    ActiveComponent,
    InactiveComponent,
    Assignment6Component,
    Assignment7Component,
    Assignment8Component,
    ChaskTsPipe,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
