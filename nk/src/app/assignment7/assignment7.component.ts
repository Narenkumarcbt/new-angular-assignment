import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-assignment7',
  templateUrl: './assignment7.component.html',
  styleUrls: ['./assignment7.component.css']
})
export class Assignment7Component implements OnInit {

  constructor() { }


  form!:FormGroup;
  ngOnInit(): void {
    this.form=new FormGroup({
      'username':new FormControl(null,Validators.required),
      'email':new FormControl(null,Validators.required),
      'password':new FormControl(null,Validators.required),
      'gender':new FormControl('female')
    })
  }

  getData(){
    console.log(this.form.value)
  }

}
